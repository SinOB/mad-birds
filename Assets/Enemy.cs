﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    //force to serialize private field
    [SerializeField] private GameObject _cloudParticlePrefab;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Bird hitBird = collision.collider.GetComponent<Bird>();

        //if we have a bird hit
        if(hitBird != null)
        {
            // instantiate the clouds at the enemy position with zero rotation
            Instantiate(_cloudParticlePrefab, transform.position, Quaternion.identity);
            //remove the enemy gameobject
            Destroy(gameObject);
            return;
        }

        Enemy enemy = collision.collider.GetComponent<Enemy>();
        if( enemy != null )
        {
            return;
        }

        //if other collision with enemy (such as by being hit by a box)
        if( collision.contacts[0].normal.y < -0.5)
        {
            //instantiate the clouds at the enemy position with zero rotation
            Instantiate(_cloudParticlePrefab, transform.position, Quaternion.identity);
            //remove the enemy gameobject
            Destroy(gameObject);
        }
            
    }
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    private static int _nextLevelIndex = 1;
    private Enemy[] _enemies;

    //called when object becomes active and enabled - from MonoBehaviour
    private void OnEnable()
    {
        //save the enemies to an array
        _enemies = FindObjectsOfType<Enemy>();
    }

    // Update is called once per frame
    void Update()
    {
        //check if any enemies remaining
        foreach(Enemy enemy in _enemies)
        {
            // if not all dead return to play
            if (enemy != null)
                return;
        }

        //if all enemies dead log a message
        Debug.Log("You killed all enemies");

        //increment to next level
        _nextLevelIndex++;
        string nextLevelName = "Level" + _nextLevelIndex;
        //load next level
        SceneManager.LoadScene(nextLevelName);

    }
}

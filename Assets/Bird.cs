﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Bird : MonoBehaviour
{
    private Vector3 _initialPosition;
    private bool _birdWasLaunched; //default is always false
    private float _timeSittingAround;

    //setup variable that can be modified during game play (in development)
    [SerializeField] private float _launchPower = 500;

    //called when the script is loaded
    private void Awake()
    {
        _initialPosition = transform.position;
    }

    // called every frame - from MonoBehaviour
    private void Update()
    {
        // set position for line to draw when pull back bird
        GetComponent<LineRenderer>().SetPosition(0, transform.position);
        GetComponent<LineRenderer>().SetPosition(1, _initialPosition);
        
        if(_birdWasLaunched && GetComponent<Rigidbody2D>().velocity.magnitude <= 0.1)
        {
            //increment by time in seconds since the last frame
            _timeSittingAround += Time.deltaTime;
        }

        //if bird has been fired outside of this range
        //or if the bird has been launched but unmoving for over 3 seconds
        if (transform.position.y > 10 ||
            transform.position.y < -10 ||
            transform.position.x > 10 ||
            transform.position.x < -15 ||
            _timeSittingAround > 3)
        {
            //reload the current scene
            string currentSceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(currentSceneName);
        }
    }

    //called when the user has pressed the mouse button down while over the Collider
    private void OnMouseDown()
    {
        //change the bird colour
        GetComponent<SpriteRenderer>().color = Color.red;
        //make the pull-back line visible
        GetComponent<LineRenderer>().enabled = true;
    }

    //called when user has released the mouse button
    private void OnMouseUp()
    {
        //reset bird back to original colour
        GetComponent<SpriteRenderer>().color = Color.white;

        Vector2 directionToInitialPosition = _initialPosition - transform.position;

        //move the bird
        GetComponent<Rigidbody2D>().AddForce(directionToInitialPosition * _launchPower);
        GetComponent<Rigidbody2D>().gravityScale = 1;
        _birdWasLaunched = true;
        //hide the pull-back line
        GetComponent<LineRenderer>().enabled = false;
    }

    //called when user has clicked on Collider and is still holding the mouse
    private void OnMouseDrag()
    {
        //set the camera to follow the action
        Vector3 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(newPosition.x, newPosition.y);
    }
}
